"""
All work and no play laTeX generator
Author : Jed Hollom
Date   : October 2017
"""

import random

phrase = 'All work and no play makes Jack a dull boy.'
chars = [' ', '', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
no_sections = int(raw_input('Number of sections: ') or 10)


# ==============================
# Typo functions
# ==============================


def cap(phrase, prob):
	"""Upper case random letter"""
	chance = random.randint(0, 100)

	if prob > chance:
		place = random.randint(0, len(phrase) - 1)
		new_char = phrase[place].upper()
		phrase = phrase[:place] + new_char + phrase[place + 1:]

	return phrase


def double(phrase, prob):
	"""Double random letter"""
	chance = random.randint(0, 100)

	if prob > chance:
		place = random.randint(0, len(phrase) - 1)
		new_char = phrase[place]
		phrase = phrase[:place] + new_char + phrase[place:]

	return phrase


def change(phrase, prob):
	"""Replace for random letter"""
	chance = random.randint(0, 100)

	if prob > chance:
		place = random.randint(0, len(phrase))
		new_char = random.randint(0, len(chars) - 1)
		phrase = phrase[:place] + chars[new_char] + phrase[place + 1:]

	return phrase


# ==============================
# Section functions
# ==============================


def text_normal():
	"""Text generation"""
	lines = random.randint(5, 30)
	output = ''
	for x in xrange(lines):
		text = phrase
		text = double(text, 40)
		text = cap(text, 20)
		text = change(text, 10)
		output = output + text + ' '

	output = output + '\\' + '\\' + '\n\n'
	return output


def text_lines():
	"""Text generation"""
	lines = random.randint(5, 30)
	output = ''
	for x in xrange(lines):
		text = phrase
		text = cap(text, 20)
		text = double(text, 40)
		text = change(text, 10)
		output = output + text + '\\\\' + '\n'

	output = output + '\\' + '\\' + '\n\n'
	return output


def text_block():
	"""Text generation"""
	lines = random.randint(5, 30)
	output = '\\begin{adjustwidth}{2cm}{2cm}\n\t'
	for x in xrange(lines):
		text = phrase
		text = cap(text, 20)
		text = double(text, 40)
		text = change(text, 10)
		output = output + text + ' '

	output = output + '\\\\' + '\n' + '\end{adjustwidth}\n\n'
	return output


# ==============================
# Generate
# ==============================
options = [
	text_normal,
	text_normal,
	text_lines,
	text_block]

# Read header
f = open('header.txt', 'r')
header = f.readlines()
f.close

# Write header
f = open('output.tex', 'w')
for line in header:
	f.write(line)

# Write content
for x in xrange(no_sections):
	option = random.randint(0, len(options) - 1)
	text = options[option]()
	f.write(text)

# Write close
f.write('\end{document}')
f.close()
